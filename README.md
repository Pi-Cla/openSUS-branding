This is a suspicious and unofficial clone of the
[openSUSE branding](https://github.com/openSUSE/branding).
There will be an effort to try to keep this up-to-date with the
upstream branding, but no guarentees...

This repo is left intentionally blank in main branch.

If you want to work on branding for a new distribution, please
create a branch based on master, copy the artwork you want to base your
work on into it and then push it as new branch to github.

Do not rebase the history - the repository becomes too large otherwise
and we need to be able to deleted old branches at times.

-----------------------------------------------------------------------

Working with this repo in GitHub
------------------------------------

On github concepts: origin links to your forked repository and we
standardized the name of the original to upstream by convention.

If you are new to Git or Codeberg, you can read [this guide](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/#cheat-sheet) alongside this one.

First, create a fork of the repo by clicking on the "fork" button in the upper right.

Then clone your fork to your PC:

    git clone https://codeberg.org/$YOUR_CODEBERG_ACCOUNT/openSUS-branding.git

and add the original repository as remote:

    cd branding
    git remote add upstream https://codeberg.org/Pi-Cla/openSUS-branding.git

Fetch the original content and checkout/merge the branch you want to work on:

    git fetch upstream
    git checkout -b tumbleweed
    git merge upstream tumbleweed

Now you can work with your local branch as you normally would.

To commit your changes to your fork, do:

    git commit -m "A useful description (eventually boo#NR) describing what changed"
(omit -m to use your $EDITOR to type the commit message instead, Linux defaults to vim)
(use -a if all the changes are relatives to the same commit)

    git push
This will update your repository fork.

Then you can create a pull request so that others can review
your submission and (hopefully) merge it in.

If you want to sync with upstream changes, do a:

    git fetch upstream
    git merge upstream tumbleweed

(this is doing manually what git pull will do when set up)

